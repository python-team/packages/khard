# -*- coding: utf-8 -*-

# tutorials:
#  - https://packaging.python.org/en/latest/distributing.html
#  - https://hynek.me/articles/sharing-your-labor-of-love-pypi-quick-and-dirty/
#  - https://gehrcke.de/2014/02/distributing-a-python-command-line-application/

from setuptools import setup

setup(test_suite="test")
